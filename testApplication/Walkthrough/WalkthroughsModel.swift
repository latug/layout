import Foundation
import UIKit

struct WalkthroughsModel {
    let image: UIImage
    let name: String
    let description: String
}
