import Foundation
import BWWalkthrough
import UIKit

final class CustomPagesBWWalkthroughViewController: BWWalkthroughPageViewController {
    
    @IBOutlet private weak var logoImageView:  UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    var walkthroughModel: WalkthroughsModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logoImageView.image = walkthroughModel?.image
        nameLabel.text = walkthroughModel?.name
        descriptionLabel.text = walkthroughModel?.description
    }
}
