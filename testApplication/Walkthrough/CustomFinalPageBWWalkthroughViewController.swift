import Foundation
import BWWalkthrough

final class CustomFinalPageBWWalkthroughViewController: BWWalkthroughPageViewController {

    @IBOutlet private weak var signInButton: UIButton!
    
    private var gradientLayer: CAGradientLayer! {
        didSet {
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0, y: 1)
            let startColor = #colorLiteral(red: 0.9892149568, green: 0.08466554433, blue: 0.1375239491, alpha: 1).cgColor
            let endColor = #colorLiteral(red: 0.82415694, green: 0.05116458982, blue: 0.1123684421, alpha: 1).cgColor
            gradientLayer.colors = [startColor, endColor]
        }
    }
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addGradientForButton()
    }
    
    override func viewDidLayoutSubviews() {
        gradientLayer.frame = signInButton.bounds
        
        signInButton.clipsToBounds = true
    }
    
    //MARK: - Design
    private func addGradientForButton() {
        gradientLayer = CAGradientLayer()
        signInButton.layer.addSublayer(gradientLayer)
    }
}
