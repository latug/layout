import UIKit

private enum State {
    case closed
    case open
}
 
extension State {
    var opposite: State {
        switch self {
        case .open: return .closed
        case .closed: return .open
        }
    }
}

final class BasketViewController: UIViewController {
    
    @IBOutlet private weak var additionalButtonView: UIView!
    @IBOutlet private weak var buttonView: UIView!
    @IBOutlet private weak var basketView: UIView!
    @IBOutlet private weak var bottomConstraint: NSLayoutConstraint!
    
    private var transitionAnimator = UIViewPropertyAnimator()
    
    private var gradientLayer: CAGradientLayer! {
        didSet {
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0, y: 1)
            let startColor = #colorLiteral(red: 0.9892149568, green: 0.08466554433, blue: 0.1375239491, alpha: 1).cgColor
            let endColor = #colorLiteral(red: 0.82415694, green: 0.05116458982, blue: 0.1123684421, alpha: 1).cgColor
            gradientLayer.colors = [startColor, endColor]
        }
    }
    
    private var currentState: State = .closed
    
    private lazy var pannedRecognizer: UIPanGestureRecognizer = {
        let recognizer = UIPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(popupViewPanned(recognizer:)))
        return recognizer
    }()
    
    private lazy var tapRecognizer: UITapGestureRecognizer = {
        let recognizer = UITapGestureRecognizer()
        recognizer.addTarget(self, action: #selector(popupViewTapped(recognizer:)))
        return recognizer
    }()
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        basketView.addGestureRecognizer(tapRecognizer)
        basketView.addGestureRecognizer(pannedRecognizer)
        
        addGradientForButton()
    }
    
    override func viewDidLayoutSubviews() {
        gradientLayer.frame = CGRect(x: 0, y: 0, width: self.buttonView.bounds.size.width, height: self.buttonView.bounds.size.height)
        buttonView.layer.cornerRadius = 20
        
        buttonView.layer.masksToBounds = true
    }
    
    //MARK: - Animation
    private func animateTransitionIfNeeded(to state: State, duration: TimeInterval) {
        
        let state = state
        transitionAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1, animations: {
            switch state {
            case .open:
                self.bottomConstraint.constant = 52
            case .closed:
                self.bottomConstraint.constant = -240
            }
            self.view.layoutIfNeeded()
        })
        transitionAnimator.addCompletion { position in
            switch position {
            case .start:
                self.currentState = state.opposite
            case .end:
                self.currentState = state
            case .current:
                ()
            @unknown default:
                break
            }
            
            switch self.currentState {
            case .open:
                self.bottomConstraint.constant = 52
            case .closed:
                self.bottomConstraint.constant = -240
            }
        }
        transitionAnimator.startAnimation()
    }
    
    @objc private func popupViewTapped(recognizer: UITapGestureRecognizer) {
        animateTransitionIfNeeded(to: currentState.opposite, duration: 0.5)
    }

    @objc private func popupViewPanned(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            animateTransitionIfNeeded(to: currentState.opposite, duration: 0.5)
            transitionAnimator.pauseAnimation()
        case .changed:
            let translation = recognizer.translation(in: basketView)
            var fraction = -translation.y / 240
            if currentState == .open { fraction *= -1 }
            transitionAnimator.fractionComplete = fraction
        case .ended:
            transitionAnimator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
        default:
            ()
        }
    }
    
    //MARK: - Design
    private func addGradientForButton() {
        gradientLayer = CAGradientLayer()
                          
        buttonView.layer.addSublayer(gradientLayer)
        buttonView.addSubview(additionalButtonView)
    }
}
