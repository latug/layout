import UIKit
import BWWalkthrough

final class CustomBWWalkthroughViewController: BWWalkthroughViewController {
    
    override func scrollViewDidScroll(_ sv: UIScrollView) {
        
        let allContent = sv.contentSize.width
        let cropPart = allContent / 4 * 2
        
        if sv.contentOffset.x > cropPart {
            pageControl?.isHidden = true
        } else {
            pageControl?.isHidden = false
        }
    }
}
