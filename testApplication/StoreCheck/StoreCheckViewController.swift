import UIKit
import MapKit

final class StoreCheckViewController: UIViewController {

    @IBOutlet private weak var mapView: MKMapView!
    @IBOutlet private weak var gradientView: UIView!
    @IBOutlet private weak var viewButton: UIButton!
    
    private var gradientLayer: CAGradientLayer! {
        didSet {
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0, y: 1)
            let startColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0).cgColor
            let endColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).cgColor
            gradientLayer.colors = [startColor, endColor]
            // gradientLayer.locations = [0, 0.3, 1]
        }
    }
    
    private var gradientLayerForButton: CAGradientLayer! {
        didSet {
            gradientLayerForButton.startPoint = CGPoint(x: 0, y: 0)
            gradientLayerForButton.endPoint = CGPoint(x: 1, y: 1)
            let firstColor = #colorLiteral(red: 0.9991982579, green: 0.4479448199, blue: 0.07564032823, alpha: 1).cgColor
            let secondColor = #colorLiteral(red: 0.8925949335, green: 0.02642426081, blue: 0.1244399622, alpha: 1).cgColor
            let thirdColor = #colorLiteral(red: 0.700730145, green: 0.02153962478, blue: 0.2590168417, alpha: 1).cgColor
            gradientLayerForButton.colors = [firstColor, secondColor, thirdColor]
            //gradientLayerForButton.locations = [0, 0.5, 1]
        }
    }
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configMapView()
        addGradientForButton()
    }
    
    override func viewDidLayoutSubviews() {
        gradientLayer.frame = CGRect(x: 0, y: 0, width: self.gradientView.bounds.size.width, height: self.gradientView.bounds.size.height)
        
        gradientLayerForButton.frame = viewButton.bounds
    }
    
    //MARK: - Design
    private func addGradientForButton() {
        gradientLayer = CAGradientLayer()
        gradientLayerForButton = CAGradientLayer()
        
        gradientView.layer.addSublayer(gradientLayer)
        viewButton.layer.addSublayer(gradientLayerForButton)
    }
    
    private func configMapView() {
        let regionRadius: CLLocationDistance = 10000
        let initialLocation = CLLocation(latitude: 52.05, longitude: 23.41)
        let coordinateRegion = MKCoordinateRegion(center: initialLocation.coordinate,
                                                  latitudinalMeters: regionRadius,
                                                  longitudinalMeters: regionRadius)
        
        mapView.setRegion(coordinateRegion, animated: true)
    }
}
