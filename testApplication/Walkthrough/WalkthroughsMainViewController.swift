import UIKit
import BWWalkthrough

final class WalkthroughsMainViewController: UIViewController, BWWalkthroughViewControllerDelegate {

    private var walkthroughs: CustomBWWalkthroughViewController!
    private var walkthroughArray = [WalkthroughsModel(image: UIImage(named: "logo")!, name: "МОБИСКАН", description: "Позволяет ускорить процесс покупки и сэкономить ваше время"),
                            WalkthroughsModel(image: UIImage(named: "qrCode")!, name: "Как начать", description: "Отсканируйте QR-код у входа в магазин"),
                            WalkthroughsModel(image: UIImage(named: "barCode")!, name: "Как пользоваться", description: "Сканируйте штрих-код товара и кладите его в пакет или корзину")
    ]
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configWalkthrough()
    }
    
    //MARK: - Config
    private func configWalkthrough() {
        let stb = UIStoryboard(name: "Main", bundle: nil)
        
        walkthroughs = (stb.instantiateViewController(withIdentifier: "container") as! CustomBWWalkthroughViewController)
        walkthroughs.delegate = self
        
        for count in 0..<walkthroughArray.count {
            let page = stb.instantiateViewController(withIdentifier: "page") as! CustomPagesBWWalkthroughViewController
            
            page.walkthroughModel = walkthroughArray[count]
            
            walkthroughs.add(viewController: page)
        }
        
        let finalPage = stb.instantiateViewController(withIdentifier: "finalPage") as! CustomFinalPageBWWalkthroughViewController
        
        walkthroughs.add(viewController: finalPage)

        let backgroundImageView = UIImageView(frame: view.bounds)
        backgroundImageView.contentMode = .scaleToFill
        backgroundImageView.image = UIImage(named: "background")
        backgroundImageView.center = walkthroughs.view.center
        
        walkthroughs.view.addSubview(backgroundImageView)
        walkthroughs.view.sendSubviewToBack(backgroundImageView)
        
        if let window = UIApplication.shared.windows.last {
            window.rootViewController = walkthroughs
        }
    }
}
